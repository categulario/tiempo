#!/bin/bash

# Fail if any of the commands fail
set -e

# Build the docs
cd docs/
make man
gzip -f build/man/tiempo.1
cd ..

# build the binary
rustup component add clippy
cargo clippy --all-targets --all-features -- -D warnings
cargo test
cargo build --locked --release

# move binary
install -Dm755 target/release/t build/bin/t
# move documentation
install -Dm644 CHANGELOG.md build/share/doc/tiempo/CHANGELOG.md
install -Dm644 README.md build/share/doc/tiempo/README.md
install -Dm644 LICENSE build/share/doc/tiempo/LICENSE
# move man page
install -Dm644 docs/build/man/tiempo.1.gz build/share/man/man1/tiempo.1.gz
# move completions
install -Dm644 completions/bash/t build/share/bash-completion/completions/t
install -Dm644 completions/fish/t.fish build/share/fish/vendor_completions.d/t.fish
install -Dm644 completions/zsh/_t build/share/zsh/site-functions/_t
# move install scripts
install -Dm755 scripts/install.sh build/install.sh
install -Dm755 scripts/uninstall.sh build/uninstall.sh

# compress the tar file
tar -cvzf tiempo-${CI_COMMIT_TAG}-x86_64.tar.gz build/
# makes the debian archive
./scripts/debpackage.sh
# computes the sums
sha256sum tiempo-${CI_COMMIT_TAG}-x86_64.tar.gz > tiempo-${CI_COMMIT_TAG}-x86_64.tar.gz.sum
sha256sum debian-package/tiempo_${CI_COMMIT_TAG}_amd64.deb > tiempo_${CI_COMMIT_TAG}_amd64.deb.sum
mkdir -p artifacts
mv tiempo-${CI_COMMIT_TAG}-x86_64.tar.gz artifacts/
mv debian-package/tiempo_${CI_COMMIT_TAG}_amd64.deb artifacts/
mv tiempo-${CI_COMMIT_TAG}-x86_64.tar.gz.sum artifacts/
mv tiempo_${CI_COMMIT_TAG}_amd64.deb.sum artifacts/
