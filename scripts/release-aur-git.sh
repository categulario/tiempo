#!/bin/bash

# Needed variables
VERSION=${CI_COMMIT_TAG:1}
PROJECT_NAME=tiempo
PROJECT_BINARY=t

echo "# Maintainer: Abraham Toriz <categulario at gmail dot com>
pkgname=$PROJECT_NAME-git
pkgver=$VERSION
pkgrel=1
pkgdesc='A command line time tracking application'
arch=('i686' 'x86_64')
url='https://gitlab.com/categulario/tiempo-rs'
license=('GPL3')
depends=()
optdepends=('sqlite: for manually editing the database')
makedepends=('cargo' 'git' 'python-sphinx' 'python-tomlkit' 'gzip' 'make')
provides=('$PROJECT_NAME')
conflicts=('$PROJECT_NAME')
source=('$PROJECT_NAME-git::git+https://gitlab.com/categulario/tiempo-rs')
sha256sums=('SKIP')

pkgver() {
    cd \"\$pkgname\"
    printf \"%s\" \"\$(git describe --long | sed 's/\([^-]*-\)g/r\1/;s/-/./g;s/v//g')\"
}

build() {
    cd \"\$pkgname\"
    cargo build --release --locked
    cd docs
    make man
    gzip build/man/$PROJECT_NAME.1
}

package() {
    cd \"\$pkgname\"
    install -Dm755 target/release/$PROJECT_BINARY \"\$pkgdir\"/usr/bin/$PROJECT_BINARY

    install -Dm644 README.md \"\$pkgdir\"/usr/share/doc/$PROJECT_NAME/README.md
    install -Dm644 LICENSE \"\$pkgdir\"/usr/share/doc/$PROJECT_NAME/LICENSE
    install -Dm644 CHANGELOG.md \"\$pkgdir\"/usr/share/doc/$PROJECT_NAME/CHANGELOG.md

    install -Dm644 docs/build/man/$PROJECT_NAME.1.gz \"\$pkgdir\"/usr/share/man/man1/$PROJECT_NAME.1.gz
    install -Dm644 completions/bash/t \"\$pkgdir\"/usr/share/bash-completion/completions/t
    install -Dm644 completions/fish/t.fish \"\$pkgdir\"/usr/share/fish/vendor_completions.d/t.fish
    install -Dm644 completions/zsh/_t \"\$pkgdir\"/usr/share/zsh/site-functions/_t

    ln -s \"\$pkgdir\"/usr/share/man/man1/$PROJECT_NAME.1.gz \"\$pkgdir\"/usr/share/man/man1/t.1.gz
}" | tee PKGBUILD > /dev/null

makepkg --printsrcinfo > .SRCINFO
