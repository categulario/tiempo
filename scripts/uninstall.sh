#!/bin/bash

rm -f /usr/bin/t

prefix=${1:-/usr}

rm -f $prefix/bin/t
rm -rf $prefix/share/doc/tiempo/
rm -f $prefix/share/fish/vendor_completions.d/t.fish
rm -f $prefix/share/bash-completion/completions/t
rm -f $prefix/share/zsh/site-functions/_t
rm -f $prefix/share/man/man1/tiempo.1.gz
