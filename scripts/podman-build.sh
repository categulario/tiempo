#!/bin/bash

if [[ -z $1 ]]; then
    echo "please pass a tag as first argument. Something like v1.6.0"
    exit 1
fi

podman run -it --rm \
    --workdir /app \
    --volume ./:/app \
    --volume tiempo_cargo_index:/usr/local/cargo/registry \
    --env CI_COMMIT_TAG=$1 \
    tiempo-build-env ./scripts/build.sh
