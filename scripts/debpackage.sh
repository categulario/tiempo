# Create a debian package for this project.
#
# This script is intended for use in a CI environment, however it can be run in
# your development machine if you pass these variables with the apropiate
# values:
#
# * CI_COMMIT_TAG=v1.2.3
#
# For example you can build and install this package in the very container it is
# done in CI using these commands:
#
# podman run -it --rm --volume=./:/src --env CI_COMMIT_TAG=v1.2.3 docker.io/categulario/rust-cli-image:latest
# cd src && cargo build --release && ./debpackage.sh
# apt install ./debian-package/tiempo_1.2.3_amd64.deb
# t --help
COPYRIGHT_YEARS="2018 - "$(date "+%Y")
DPKG_STAGING="debian-package"
DPKG_DIR="${DPKG_STAGING}/dpkg"

PROJECT_MANTAINER="Abraham Toriz Cruz"
PROJECT_HOMEPAGE="https://gitlab.com/categulario/tiempo-rs"
PROJECT_NAME=tiempo
PROJECT_BINARY=t
PROJECT_DESCRIPTION="A command line time tracking application"

mkdir -p "${DPKG_DIR}"

DPKG_BASENAME=${PROJECT_NAME}
DPKG_CONFLICTS=
DPKG_VERSION=${CI_COMMIT_TAG:1}
DPKG_ARCH=amd64
DPKG_NAME="${DPKG_BASENAME}_${CI_COMMIT_TAG}_${DPKG_ARCH}.deb"
DPKG_DEPENDS=
DPKG_SECTION=utils

# Binary
install -Dm755 "target/release/$PROJECT_BINARY" "${DPKG_DIR}/usr/bin/$PROJECT_BINARY"
# README and LICENSE
install -Dm644 "README.md" "${DPKG_DIR}/usr/share/doc/${DPKG_BASENAME}/README.md"
install -Dm644 "LICENSE" "${DPKG_DIR}/usr/share/doc/${DPKG_BASENAME}/LICENSE"
install -Dm644 "CHANGELOG.md" "${DPKG_DIR}/usr/share/doc/${DPKG_BASENAME}/CHANGELOG.md"
install -Dm644 "docs/build/man/$PROJECT_NAME.1.gz" "${DPKG_DIR}/usr/share/man/man1/$PROJECT_NAME.1.gz"
install -Dm644 "completions/bash/t" "${DPKG_DIR}/usr/share/bash-completion/completions/t"
install -Dm644 "completions/fish/t.fish" "${DPKG_DIR}/usr/share/fish/vendor_completions.d/t.fish"
install -Dm644 "completions/zsh/_t" "${DPKG_DIR}/usr/share/zsh/site-functions/_t"

cat > "${DPKG_DIR}/usr/share/doc/${DPKG_BASENAME}/copyright" <<EOF
Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: $PROJECT_NAME
Source: $PROJECT_HOMEPAGE
Files: *
Copyright: $PROJECT_MANTAINER
Copyright: $COPYRIGHT_YEARS $PROJECT_MANTAINER
License: GPL
EOF

chmod 644 "${DPKG_DIR}/usr/share/doc/${DPKG_BASENAME}/copyright"

mkdir -p "${DPKG_DIR}/DEBIAN"

# control file
cat > "${DPKG_DIR}/DEBIAN/control" <<EOF
Package: ${DPKG_BASENAME}
Version: ${DPKG_VERSION}
Section: ${DPKG_SECTION}
Priority: optional
Maintainer: ${PROJECT_MANTAINER}
Homepage: ${PROJECT_HOMEPAGE}
Architecture: ${DPKG_ARCH}
Provides: ${PROJECT_NAME}
Depends: ${DPKG_DEPENDS}
Conflicts: ${DPKG_CONFLICTS}
Description: ${PROJECT_DESCRIPTION}
EOF
DPKG_PATH="${DPKG_STAGING}/${DPKG_NAME}"
# build dpkg
fakeroot dpkg-deb --build "${DPKG_DIR}" "${DPKG_PATH}"
