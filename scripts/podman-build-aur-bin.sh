#!/bin/bash

if [[ -z $1 ]]; then
    echo "please pass a tag as first argument. Something like v1.6.0"
    exit 1
fi

podman run -it --rm \
    --workdir /app \
    --volume ./:/app \
    --env CI_COMMIT_TAG=$1 \
    --env CI_PROJECT_ID=27545092 \
    --uidmap 1000:0:1 --uidmap 0:1:1000 \
    categulario/makepkg ./scripts/release-aur-bin.sh
