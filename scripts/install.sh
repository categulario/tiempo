#!/bin/bash

__dir=$(dirname $(realpath $0))
prefix=${1:-/usr}

install -Dm755 $__dir/bin/t $prefix/bin/t
install -Dm644 $__dir/share/fish/vendor_completions.d/t.fish $prefix/share/fish/vendor_completions.d/t.fish
install -Dm644 $__dir/share/doc/tiempo/README.md $prefix/share/doc/tiempo/README.md
install -Dm644 $__dir/share/doc/tiempo/CHANGELOG.md $prefix/share/doc/tiempo/CHANGELOG.md
install -Dm644 $__dir/share/doc/tiempo/LICENSE $prefix/share/doc/tiempo/LICENSE
install -Dm644 $__dir/share/bash-completion/completions/t $prefix/share/bash-completion/completions/t
install -Dm644 $__dir/share/zsh/site-functions/_t $prefix/share/zsh/site-functions/_t
install -Dm644 $__dir/share/man/man1/tiempo.1.gz $prefix/share/man/man1/tiempo.1.gz
ln -s $prefix/share/man/man1/tiempo.1.gz $prefix/share/man/man1/t.1.gz
