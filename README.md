# Tiempo

A [timetrap](https://github.com/samg/timetrap/) compatible command line time
tracking application. [Read the fine manual](https://tiempo.categulario.xyz).
Chat in the matrix room
[#tiempo:matrix.cuates.net](https://matrix.to/#/#tiempo:matrix.cuates.net).

## Installation

### For Archlinux (and derivatives) users

There are both binary and source packages in the AUR:

* [tiempo-bin](https://aur.archlinux.org/packages/tiempo-bin)
* [tiempo-git](https://aur.archlinux.org/packages/tiempo-git)

### For all other linux users

Go to [gitlab releases page](https://gitlab.com/categulario/tiempo-rs/-/releases)
and grab the latest binary for your linux. There is a `.deb` file for Debian and
Ubuntu as well as a binary for any `x86_64` Linux.

In the case of the tar archive you just need to run the included `install.sh`
script.

### For Rust developers

You have `cargo`! you can run:

    cargo install tiempo

However that will not install the beautiful man page. Although you can still see
it at https://tiempo.categulario.xyz .

### For everyone else

You need to compile `tiempo` by yourself. But don't worry! It is not that hard.
Just clone [the repository](https://gitlab.com/categulario/tiempo-rs), make sure
you have [rust installed](https://rustup.rs) and run:

    cargo build --release

inside the repository. The binary will be named `t` (or `t.exe` if you use
windows) and it is located inside the `target/release` directory that was
created during compilation.

## How to build

You need [rust](https://rustup.rs), then clone the repo and simply run

    cargo test

to check that everything is working, and then

    cargo build --release

to have a binary at `target/release/t` that you can then move to a
directory in your `PATH` or use it by its absoulte or relative paths.

Run

    t --help

to see the options.

### Development database

When developing I prefer not to mess with my own database, so I use this `.env`
file:

    export TIMETRAP_CONFIG_FILE=/absolute/path/to/repo/dev_config.toml
    PS1="$ "

and when I want to test some commands against such config file I just source it:

    source .env
    cargo run -- in 'hola'

### Documentation

The docs are written using [sphinx](https://www.sphinx-doc.org/en/master/). To
install the required dependencies enter the `docs` directory and create a virual
environment:

    virtualenv .venv

then activate it and install the dependencies:

    source .venv/bin/activate
    pip install -r requirements.txt

To build the docs just do:

    make html

for the html version (output located at `docs/build/html`), or

    make man

for the man page (output located at `docs/build/man/tiempo.1`). To test the man
page you can do:

    man -l build/man/tiempo.1

To get a live-reloaded server with the html docs do:

    sphinx-autobuild source/ build/html/

The contents of the man page are located in `docs/source/index.rst`,
formatted as
[reStructuredText](https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html).

### Building the packages like in CI but locally

First pull the image:

    podman pull tiempo-build-env

Or build it yourself from this repo:

    podman build -t docker.io/categulario/tiempo-build-env .

Then build the artifacts:

    ./scripts/podman-build.sh

To build the archlinux PKGBUILDs (depends on the artifacts folder created by the
previous command):

    ./scripts/podman-build-aur-bin.sh
    ./scripts/podman-build-aur-git.sh

Both of the previous commands produce PKGBUILDs in the current directory so if
you run both sequentially you'll lose the PKGBUILD of the first command.

## Special Thanks

To [timetrap](https://github.com/samg/timetrap) for existing, to
[samg](https://github.com/samg) for creating it. It is the tool I was looking
for and whose design I took as reference, keeping compatibility when possible.

## Release checklist

(mostly to remind myself)

* [ ] Ensure tests pass and that clippy doesn't complain
* [ ] Add documentation about the new features
* [ ] Create an entry in `CHANGELOG.md` with the target version, commit it
* [ ] run `vbump`
* [ ] git push && git push --tags && cargo publish
* [ ] edit the release in gitlab to include its changelog.
