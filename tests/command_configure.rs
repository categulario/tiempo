extern crate testutils;

use testutils::run_command;

use pretty_assertions::assert_str_eq;

// this test was needed because calling `t c` complained about auto_checkout
// being requested as a clap argument but it was not actually declared in cli.rs
//
// so the test ensures that a simple invocation of this subcommand always works
#[test]
fn test_configure_just_works() {
    let output = run_command!{
        config: "tests/assets/config/empty.toml",
        args: ["configure"],
    };

    assert_str_eq!(String::from_utf8_lossy(&output.stderr), "");
    assert_str_eq!(String::from_utf8_lossy(&output.stdout), "tests/assets/config/empty.toml\n");
    assert_eq!(output.status.code().unwrap(), 0);
}

#[test]
fn set_formatter_paths() {
    std::fs::create_dir_all("tests/assets/ignoreme").unwrap();
    std::fs::copy(
        "tests/assets/config/set_formatter_paths.toml",
        "tests/assets/ignoreme/set_formatter_paths.toml",
    ).unwrap();
    let output = run_command! {
        config: "tests/assets/ignoreme/set_formatter_paths.toml",
        args: ["configure", "--formatter-search-path", "path/1", "--formatter-search-path", "path/2"]
    };

    assert_str_eq!(String::from_utf8_lossy(&output.stderr), "");
    assert_eq!(output.status.code().unwrap(), 0);

    assert_str_eq!(
        std::fs::read_to_string("tests/assets/ignoreme/set_formatter_paths.toml").unwrap(),
        include_str!("assets/config/set_formatter_paths__edited.toml")
    );
}
