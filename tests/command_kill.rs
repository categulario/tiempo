extern crate testutils;

use std::fs::File;

use testutils::run_command;

use pretty_assertions::assert_str_eq;

#[test]
fn no_args_fails() {
    let output = run_command!{
        config: "tests/assets/config/empty.toml",
        args: ["kill"],
    };

    assert_str_eq!(String::from_utf8_lossy(&output.stderr), "\
error: the following required arguments were not provided:
  <--id <ID>|--sheet <SHEET>|--last>

Usage: t kill <--id <ID>|--sheet <SHEET>|--last>

For more information, try '--help'.
");
    assert_str_eq!(String::from_utf8_lossy(&output.stdout), "");
    assert_eq!(output.status.code().unwrap(), 2);
}

#[test]
fn arg_id_works() {
    let output = run_command!{
        config: "tests/assets/config/empty.toml",
        args: ["k", "--id", "1"],
        stdin: File::open("tests/assets/in/no").unwrap(),
    };

    assert_str_eq!(String::from_utf8_lossy(&output.stderr), "");
    assert_str_eq!(String::from_utf8_lossy(&output.stdout), "\
There's no entry with id 1. Someone found it before we did.
");
    assert_eq!(output.status.code().unwrap(), 0);
}

#[test]
fn arg_sheet_works() {
    let output = run_command!{
        config: "tests/assets/config/empty.toml",
        args: ["k", "--sheet", "foo"],
        stdin: File::open("tests/assets/in/no").unwrap(),
    };

    assert_str_eq!(String::from_utf8_lossy(&output.stderr), "");
    assert_str_eq!(String::from_utf8_lossy(&output.stdout), "\
are you sure you want to delete 0 entries on sheet \"foo\"? [y/N] Don't worry, they're still there
");
    assert_eq!(output.status.code().unwrap(), 0);
}

#[test]
fn arg_last_works() {
    let output = run_command!{
        config: "tests/assets/config/empty.toml",
        args: ["k", "--last"],
        stdin: File::open("tests/assets/in/no").unwrap(),
    };

    assert_str_eq!(String::from_utf8_lossy(&output.stderr), "");
    assert_str_eq!(String::from_utf8_lossy(&output.stdout), "\
Nothing to delete
");
    assert_eq!(output.status.code().unwrap(), 0);
}

#[test]
fn all_but_id_fails() {
    let output = run_command!{
        config: "tests/assets/config/empty.toml",
        args: ["k", "--last", "--sheet", "foo"],
        stdin: File::open("tests/assets/in/no").unwrap(),
    };

    assert_str_eq!(String::from_utf8_lossy(&output.stderr), "\
error: the argument '--last' cannot be used with one or more of the other specified arguments

Usage: t kill <--id <ID>|--sheet <SHEET>|--last>

For more information, try '--help'.
");
    assert_str_eq!(String::from_utf8_lossy(&output.stdout), "");
    assert_eq!(output.status.code().unwrap(), 2);
}

#[test]
fn all_but_last_fails() {
    let output = run_command!{
        config: "tests/assets/config/empty.toml",
        args: ["k", "--id", "1", "--sheet", "foo"],
        stdin: File::open("tests/assets/in/no").unwrap(),
    };

    assert_str_eq!(String::from_utf8_lossy(&output.stderr), "\
error: the argument '--id <ID>' cannot be used with one or more of the other specified arguments

Usage: t kill <--id <ID>|--sheet <SHEET>|--last>

For more information, try '--help'.
");
    assert_str_eq!(String::from_utf8_lossy(&output.stdout), "");
    assert_eq!(output.status.code().unwrap(), 2);
}

#[test]
fn all_but_sheet_fails() {
    let output = run_command!{
        config: "tests/assets/config/empty.toml",
        args: ["k", "--id", "1", "--last"],
        stdin: File::open("tests/assets/in/no").unwrap(),
    };

    assert_str_eq!(String::from_utf8_lossy(&output.stderr), "\
error: the argument '--id <ID>' cannot be used with one or more of the other specified arguments

Usage: t kill <--id <ID>|--sheet <SHEET>|--last>

For more information, try '--help'.
");
    assert_str_eq!(String::from_utf8_lossy(&output.stdout), "");
    assert_eq!(output.status.code().unwrap(), 2);
}

#[test]
fn all_three_fail() {
    let output = run_command!{
        config: "tests/assets/config/empty.toml",
        args: ["k", "--id", "1", "--last", "--sheet", "foo"],
        stdin: File::open("tests/assets/in/no").unwrap(),
    };

    assert_str_eq!(String::from_utf8_lossy(&output.stderr), "\
error: the argument '--id <ID>' cannot be used with one or more of the other specified arguments

Usage: t kill <--id <ID>|--sheet <SHEET>|--last>

For more information, try '--help'.
");
    assert_str_eq!(String::from_utf8_lossy(&output.stdout), "");
    assert_eq!(output.status.code().unwrap(), 2);
}
