use clap::{
    Parser, Subcommand, command,
};

use crate::commands::{
    archive, backend, configure, display, edit, kill, list, month, now, out,
    r#in, resume, sheet, today, week, yesterday, migrate,
};

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
    #[command(subcommand)]
    pub subcommand: Subcli,
}

#[derive(Subcommand)]
pub enum Subcli {
    #[command(visible_alias="a")]
    Archive(archive::Cli),

    #[command(visible_alias="b")]
    Backend(backend::Cli),

    #[command(visible_alias="c")]
    Configure(configure::Cli),

    #[command(visible_alias="d")]
    Display(display::Cli),

    #[command(visible_alias="e")]
    Edit(edit::Cli),

    #[command(visible_alias="k")]
    Kill(kill::Cli),

    #[command(visible_alias="l")]
    List(list::Cli),

    #[command()]
    Migrate(migrate::Cli),

    #[command(visible_alias="m")]
    Month(month::Cli),

    #[command(visible_alias="n")]
    Now(now::Cli),

    #[command(visible_alias="o")]
    Out(out::Cli),

    #[command(visible_alias="i")]
    In(r#in::Cli),

    #[command(visible_alias="r")]
    Resume(resume::Cli),

    #[command(visible_alias="s")]
    Sheet(sheet::Cli),

    #[command(visible_alias="t")]
    Today(today::Cli),

    #[command(visible_alias="w")]
    Week(week::Cli),

    #[command(visible_alias="y")]
    Yesterday(yesterday::Cli),
}
