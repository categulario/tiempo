use std::io::{BufRead, Write};

use clap::Args;
use chrono::{DateTime, Utc};

use crate::error::Result;
use crate::timeparse::parse_time;
use crate::database::Database;
use crate::io::Streams;

use crate::interactive::note_from_last_entries;
use super::{Command, Facts, r#in, sheet};

/// Restart the timer for an entry. Defaults to the last active entry
#[derive(Args)]
pub struct Cli {
    /// Use this time instead of now
    #[arg(long, value_name="TIME", value_parser=parse_time)]
    at: Option<DateTime<Utc>>,

    /// Use entry with ID instead of the last entry
    #[arg(long, value_name="ID")]
    id: Option<u64>,

    /// Choose an entry of the (unique) last N interactively
    #[arg(short, long, conflicts_with="id")]
    interactive: bool,
}

#[derive(Default)]
enum SelectedEntry {
    Id(u64),
    Interactive,
    #[default]
    NotSpecified,
}

#[derive(Default)]
struct InternalArgs {
    entry: SelectedEntry,
    at: Option<DateTime<Utc>>,
}

impl From<Cli> for InternalArgs {
    fn from(cli: Cli) -> InternalArgs {
        let entry = if let Some(id) = cli.id {
            SelectedEntry::Id(id)
        } else if cli.interactive {
            SelectedEntry::Interactive
        } else {
            SelectedEntry::NotSpecified
        };

        InternalArgs {
            at: cli.at,
            entry,
        }
    }
}

fn resume<D, I, O, E>(args: InternalArgs, streams: &mut Streams<D, I, O, E>, facts: &Facts, note: String) -> Result<()>
where
    D: Database,
    I: BufRead,
    O: Write,
    E: Write,
{
    writeln!(
        streams.out,
        "Resuming \"{note}\"",
    )?;

    r#in::Cli {
        at: args.at,
        note: Some(note),
    }.handle(streams, facts)
}

impl Command for Cli {
    fn handle<D, I, O, E>(self, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        InternalArgs::from(self).handle(streams, facts)
    }
}

impl Command for InternalArgs {
    fn handle<D, I, O, E>(self, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        let current_sheet = streams.db.current_sheet()?;

        // First try to process using the given id
        match self.entry {
            SelectedEntry::Id(entry_id) => {
                if let Some(entry) = streams.db.entry_by_id(entry_id)? {
                    if entry.sheet != current_sheet {
                        // first swith to the sheet
                        sheet::Cli {
                            sheet: Some(entry.sheet.clone()),
                        }.handle(streams, facts)?;
                    }

                    resume(self, streams, facts, entry.note.unwrap_or_default())
                } else {
                    writeln!(streams.out, "The entry with id '{}' could not be found to be resumed. Perhaps it was deleted?", entry_id)?;

                    Ok(())
                }
            }
            SelectedEntry::Interactive => {
                if let Some(note) = note_from_last_entries(streams, facts, &current_sheet)? {
                    resume(self, streams, facts, note)
                } else {
                    Ok(())
                }
            }
            SelectedEntry::NotSpecified => {
                // No id specified, try to find something suitable to switch to in the
                // database
                if let Some(entry) = streams.db.last_checkout_of_sheet(&current_sheet)? {
                    resume(self, streams, facts, entry.note.unwrap_or_default())
                } else {
                    writeln!(streams.out, "No entry to resume in the sheet '{}'. Perhaps start a new one?
Hint: use t in", current_sheet)?;

                    Ok(())
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use chrono::Duration;
    use pretty_assertions::{assert_eq, assert_str_eq};

    use super::*;

    #[test]
    fn resume_an_entry() {
        let args = InternalArgs::default();
        let mut streams = Streams::fake(b"");
        let facts = Facts::new();
        let one_hour_ago = facts.now - Duration::hours(1);
        let two_hours_ago = facts.now - Duration::hours(2);

        streams.db.entry_insert(two_hours_ago, Some(one_hour_ago), Some("fake note".into()), "default").unwrap();

        assert_eq!(streams.db.entries_full(None, None).unwrap().len(), 1);

        args.handle(&mut streams, &facts).unwrap();

        let all_entries = streams.db.entries_full(None, None).unwrap();

        assert_eq!(all_entries.len(), 2);

        assert_eq!(all_entries[1].id, 2);
        assert_eq!(all_entries[1].start, facts.now);
        assert_eq!(all_entries[1].end, None);
        assert_eq!(all_entries[1].note, Some("fake note".into()));
        assert_eq!(all_entries[1].sheet, "default");

        assert_str_eq!(&String::from_utf8_lossy(&streams.out), "Resuming \"fake note\"
Checked into sheet \"default\".\n");
        assert_str_eq!(&String::from_utf8_lossy(&streams.err), "");
    }

    #[test]
    fn no_entries_to_resume() {
        let args = InternalArgs::default();
        let mut streams = Streams::fake(b"");
        let facts = Facts::new();

        args.handle(&mut streams, &facts).unwrap();

        assert_str_eq!(&String::from_utf8_lossy(&streams.out), "\
No entry to resume in the sheet 'default'. Perhaps start a new one?
Hint: use t in
");
    }
}
