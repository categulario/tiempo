use std::io::{BufRead, Write};

use clap::Args;
use chrono::{Utc, Duration, Local};
use itertools::Itertools;
use ansi_term::Style;

use crate::error::Result;
use crate::database::Database;
use crate::tabulate::{Tabulate, Col, Align::*};
use crate::formatters::text::format_duration;
use crate::models::Entry;
use crate::old::{entries_or_warning, warn_if_needed};
use crate::io::Streams;

use super::{Command, Facts};

/// List existing sheets
#[derive(Default, Args)]
pub struct Cli {
    ///List archive sheets also
    #[arg(short, long)]
    all: bool,

    /// show only the sheet names
    #[arg(short, long)]
    flat: bool,
}

impl Command for Cli {
    fn handle<D, I, O, E>(self, streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        let local_today = facts.now
            .with_timezone(&Local).date_naive().and_hms_opt(0, 0, 0).unwrap()
            .and_local_timezone(Local).unwrap();
        let today = local_today.with_timezone(&Utc);
        let entries = if self.all {
            streams.db.entries_full(None, None)?
        } else {
            streams.db.entries_all_visible(None, None)?
        };
        let (mut entries, needs_warning) = entries_or_warning(entries, &streams.db)?;
        let current = streams.db.current_sheet()?;
        let last = streams.db.last_sheet()?;

        // introducte two fake entries to make both current and last show up
        entries.push(Entry {
            id: 1, sheet: current.clone(), start: facts.now, end: Some(facts.now), note: None,
        });

        entries.sort_unstable_by_key(|e| e.sheet.clone());

        if self.flat {
            let sheets: Vec<_> = entries
                .into_iter()
                .map(|e| e.sheet)
                .unique()
                .collect();

            streams.out.write_all(sheets.join("\n").as_bytes())?;
            streams.out.write_all(b"\n")?;
        } else {
            let mut total_running = Duration::seconds(0);
            let mut total_today = Duration::seconds(0);
            let mut total = Duration::seconds(0);

            let sheets: Vec<_> = entries
                .into_iter()
                .group_by(|e| e.sheet.clone())
                .into_iter()
                .map(|(key, group)| {
                    let entries: Vec<_> = group.into_iter().collect();
                    let s_running = facts.now - entries.iter().find(|e| e.end.is_none()).map(|e| e.start).unwrap_or(facts.now);
                    let s_today = entries.iter().filter(|e| e.start > today).fold(Duration::seconds(0), |acc, e| {
                        acc + (e.end.unwrap_or(facts.now) - e.start)
                    });
                    let s_total = entries.into_iter().fold(Duration::seconds(0), |acc, e| {
                        acc + (e.end.unwrap_or(facts.now) - e.start)
                    });

                    total_running += s_running;
                    total_today += s_today;
                    total += s_total;

                    (
                        if current == key {
                            "*"
                        } else if last.as_ref() == Some(&key) {
                            "-"
                        } else {
                            ""
                        },

                        key,

                        format_duration(s_running),

                        format_duration(s_today),

                        format_duration(s_total),
                    )
                })
                .collect();

            let mut tabs = Tabulate::with_columns(vec![
                // indicator of current or prev sheet
                Col::new().min_width(1).and_alignment(Right),

                // sheet name
                Col::new().min_width(9).and_alignment(Left),

                // running time
                Col::new().min_width(9).and_alignment(Right)
                    .color_if(Style::new().dimmed(), |s| s == "0:00:00")
                    .color_if(Style::new().bold(), |s| s != "0:00:00"),

                // today
                Col::new().min_width(9).and_alignment(Right)
                    .color_if(Style::new().dimmed(), |s| s == "0:00:00")
                    .color_if(Style::new().bold(), |s| s != "0:00:00"),

                // accumulated
                Col::new().min_width(12).and_alignment(Right),
            ]);

            tabs.feed(vec!["", "Timesheet", "Running", "Today", "Total Time"]);
            tabs.separator(' ');

            for sheet in sheets {
                tabs.feed(vec![sheet.0.to_string(), sheet.1, sheet.2, sheet.3, sheet.4]);
            }

            tabs.separator('-');

            tabs.feed(vec![
                "".to_string(),
                "".to_string(),
                format_duration(total_running),
                format_duration(total_today),
                format_duration(total),
            ]);

            streams.out.write_all(tabs.print(facts.env.stdout_is_tty).as_bytes())?;
        }

        warn_if_needed(&mut streams.err, needs_warning, &facts.env)?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use chrono::{Utc, TimeZone, Duration, Timelike};
    use pretty_assertions::assert_str_eq;

    use crate::database::{SqliteDatabase, Database};

    use super::*;

    #[test]
    fn list_sheets() {
        std::env::set_var("TZ", "CST+6");

        let args = Cli::default();
        let mut streams = Streams::fake(b"");

        streams.db.set_current_sheet("sheet2").unwrap();
        streams.db.set_last_sheet("sheet4").unwrap();

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 1, 1, 0, 0, 0).unwrap(), Some(Utc.with_ymd_and_hms(2021, 1, 1, 1, 0, 0).unwrap()), None, "_archived").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 1, 1, 0, 0, 0).unwrap(), Some(Utc.with_ymd_and_hms(2021, 1, 1, 10,13, 55).unwrap()), None, "sheet1").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 1, 1, 0, 0, 0).unwrap(), Some(Utc.with_ymd_and_hms(2021, 1, 1, 7, 39, 18).unwrap()), None, "sheet3").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 1, 1, 12, 0, 0).unwrap(), Some(Utc.with_ymd_and_hms(2021, 1, 1, 13, 52, 45).unwrap()), None, "sheet3").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 1, 1, 12, 0, 0).unwrap(), None, None, "sheet4").unwrap();

        let now = Utc.with_ymd_and_hms(2021, 1, 1, 13, 52, 45).unwrap();
        let facts = Facts::new().with_now(now);

        args.handle(&mut streams, &facts).unwrap();

        assert_str_eq!(&String::from_utf8_lossy(&streams.out), "  Timesheet   Running     Today   Total Time

  sheet1      0:00:00   0:00:00     10:13:55
* sheet2      0:00:00   0:00:00      0:00:00
  sheet3      0:00:00   1:52:45      9:32:03
- sheet4      1:52:45   1:52:45      1:52:45
--------------------------------------------
              1:52:45   3:45:30     21:38:43
");

        // now show all the sheets
        streams.reset_io();

        let args = Cli {
            all: true,
            ..Default::default()
        };

        args.handle(&mut streams, &facts).unwrap();

        assert_str_eq!(&String::from_utf8_lossy(&streams.out), "  Timesheet   Running     Today   Total Time

  _archived   0:00:00   0:00:00      1:00:00
  sheet1      0:00:00   0:00:00     10:13:55
* sheet2      0:00:00   0:00:00      0:00:00
  sheet3      0:00:00   1:52:45      9:32:03
- sheet4      1:52:45   1:52:45      1:52:45
--------------------------------------------
              1:52:45   3:45:30     22:38:43
");
    }

    #[test]
    fn old_database() {
        let args = Cli::default();
        let mut streams = Streams::fake(b"").with_db(
            SqliteDatabase::from_path("tests/assets/db/test_list_old_database.db").unwrap()
        );

        let now = Local.with_ymd_and_hms(2021, 7, 16, 11, 30, 45).unwrap();
        let facts = Facts::new().with_now(now.with_timezone(&Utc));

        args.handle(&mut streams, &facts).unwrap();

        assert_str_eq!(&String::from_utf8_lossy(&streams.out), "  Timesheet   Running     Today   Total Time

* default     0:10:24   0:10:26      0:10:26
--------------------------------------------
              0:10:24   0:10:26      0:10:26
");

        assert_str_eq!(
            String::from_utf8_lossy(&streams.err),
            "[WARNING] You are using the old timetrap format, it is advised that you update your database using t migrate. To supress this warning set TIEMPO_SUPRESS_TIMETRAP_WARNING=1\n"
        );
    }

    #[test]
    fn flat_display() {
        std::env::set_var("TZ", "CST+6");

        let mut streams = Streams::fake(b"");

        streams.db.set_current_sheet("sheet2").unwrap();
        streams.db.set_last_sheet("sheet4").unwrap();

        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 1, 1, 0, 0, 0).unwrap(), Some(Utc.with_ymd_and_hms(2021, 1, 1, 1, 0, 0).unwrap()), None, "_archived").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 1, 1, 0, 0, 0).unwrap(), Some(Utc.with_ymd_and_hms(2021, 1, 1, 10,13, 55).unwrap()), None, "sheet1").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 1, 1, 0, 0, 0).unwrap(), Some(Utc.with_ymd_and_hms(2021, 1, 1, 7, 39, 18).unwrap()), None, "sheet3").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 1, 1, 12, 0, 0).unwrap(), Some(Utc.with_ymd_and_hms(2021, 1, 1, 13, 52, 45).unwrap()), None, "sheet3").unwrap();
        streams.db.entry_insert(Utc.with_ymd_and_hms(2021, 1, 1, 12, 0, 0).unwrap(), None, None, "sheet4").unwrap();

        let now = Utc.with_ymd_and_hms(2021, 1, 1, 13, 52, 45).unwrap();
        let facts = Facts::new().with_now(now);
        let args = Cli {
            flat: true,
            ..Default::default()
        };

        args.handle(&mut streams, &facts).unwrap();

        assert_str_eq!(&String::from_utf8_lossy(&streams.out), "sheet1\nsheet2\nsheet3\nsheet4\n");

        let facts = Facts::new().with_now(now);
        let args = Cli {
            flat: true,
            all: true,
        };
        streams.out.clear();

        args.handle(&mut streams, &facts).unwrap();

        assert_str_eq!(&String::from_utf8_lossy(&streams.out), "_archived\nsheet1\nsheet2\nsheet3\nsheet4\n");
    }

    #[test]
    fn today_is_properly_calculated_when_utc_offset_and_old_database() {
        std::env::set_var("TZ", "CST+6");

        let mut streams = Streams::fake(b"");

        // 9 pm yesterday
        let yesterday = (Local::now().with_hour(21).unwrap().with_minute(0).unwrap() - Duration::hours(24)).with_timezone(&Utc);

        streams.db.entry_insert(yesterday, Some(yesterday + Duration::hours(1)), None, "default").unwrap();

        let facts = Facts::new();
        let args = Cli::default();

        args.handle(&mut streams, &facts).unwrap();

        assert_str_eq!(&String::from_utf8_lossy(&streams.out), "  Timesheet   Running     Today   Total Time

* default     0:00:00   0:00:00      1:00:00
--------------------------------------------
              0:00:00   0:00:00      1:00:00
");
    }
}
