use std::process::{self, Stdio};
use std::io::{BufRead, Write};

use clap::Args;

use crate::error::Result;
use crate::database::Database;
use crate::error::Error::*;
use crate::commands::Command;
use crate::io::Streams;

use super::Facts;

/// Open an sqlite shell to the database.
#[derive(Args)]
pub struct Cli;

impl Command for Cli {
    fn handle<D, I, O, E>(self, _streams: &mut Streams<D, I, O, E>, facts: &Facts) -> Result<()>
    where
        D: Database,
        I: BufRead,
        O: Write,
        E: Write,
    {
        let status = process::Command::new("sqlite3")
            .arg(&facts.config.database_file)
            .stdin(Stdio::inherit())
            .stdout(Stdio::inherit())
            .stderr(Stdio::inherit())
            .output().map_err(Sqlite3CommandFailed)?
            .status;

        if status.success() {
            Ok(())
        } else {
            Err(Sqlite3CommandFailedUnkown)
        }
    }
}
