# tiempo does not accept files as arguments
complete --command t --no-files

complete --command t -n "__fish_use_subcommand" -s h -l help -d -'Print help information'
complete --command t -n "__fish_use_subcommand" -s V -l version -d 'Print version information'
complete --command t -n "__fish_use_subcommand" -f -a "archive" -d 'Move entries to a hidden sheet (by default named \'_[SHEET]\') so they\'re out of the way.'
complete --command t -n "__fish_use_subcommand" -f -a "backend" -d 'Open an sqlite shell to the database.'
complete --command t -n "__fish_use_subcommand" -f -a "configure" -d 'Configure tiempo in-place. If no arguments are given it just prints the path to the config file in use.'
complete --command t -n "__fish_use_subcommand" -f -a "display" -d 'Display the current timesheet or a specific. Pass `all\' as SHEET to display all unarchived sheets or `full\' to display archived and unarchived sheets.'
complete --command t -n "__fish_use_subcommand" -f -a "today" -d 'Display entries that started today'
complete --command t -n "__fish_use_subcommand" -f -a "yesterday" -d 'Display entries that started yesterday'
complete --command t -n "__fish_use_subcommand" -f -a "week" -d 'Display entries starting last monday or later'
complete --command t -n "__fish_use_subcommand" -f -a "month" -d 'Display entries starting this month'
complete --command t -n "__fish_use_subcommand" -f -a "in" -d 'Start an activity in the current timesheet'
complete --command t -n "__fish_use_subcommand" -f -a "resume" -d 'Restart the timer for an entry. Defaults to the last active entry'
complete --command t -n "__fish_use_subcommand" -f -a "out" -d 'end the active entry in the current timesheet'
complete --command t -n "__fish_use_subcommand" -f -a "sheet" -d 'Change active timesheet or list existing timesheets'
complete --command t -n "__fish_use_subcommand" -f -a "list" -d 'List existing sheets'
complete --command t -n "__fish_use_subcommand" -f -a "kill" -d 'Delete an entry or an entire sheet'
complete --command t -n "__fish_use_subcommand" -f -a "now" -d 'Show all running entries'
complete --command t -n "__fish_use_subcommand" -f -a "edit" -d 'Edit an entry'
complete --command t -n "__fish_use_subcommand" -f -a "help" -d 'Print this message or the help of the given subcommand(s)'
complete --command t -n "__fish_seen_subcommand_from archive" -s s -l start -d 'Include entries that start on this date or later' -r
complete --command t -n "__fish_seen_subcommand_from archive" -s e -l end -d 'Include entries that start on this date or earlier' -r
complete --command t -n "__fish_seen_subcommand_from archive" -s g -l grep -d 'Only include entries whose note matches this regular expression' -r
complete --command t -n "__fish_seen_subcommand_from archive" -s t -l time -d 'Time in hours to archive. Archived time will be equal or less than this.' -r
complete --command t -n "__fish_seen_subcommand_from archive" -s f -l fake -d 'Don\'t actually archive the entries, just display them'
complete --command t -n "__fish_seen_subcommand_from archive" -s h -l help -d 'Print help information'
complete --command t -n "__fish_seen_subcommand_from backend" -s h -l help -d 'Print help information'
complete --command t -n "__fish_seen_subcommand_from configure" -l round-in-seconds -d 'The duration of time to use for rounding with the -r flag. Default: 900 (15 m)' -r
complete --command t -n "__fish_seen_subcommand_from configure" -l database-file -d 'The file path of the sqlite database' -r
complete --command t -n "__fish_seen_subcommand_from configure" -l append-notes-delimiter -d 'delimiter used when appending notes via t edit --append. Default: \' \' (space)' -r
complete --command t -n "__fish_seen_subcommand_from configure" -l formatter-search-paths -d 'comma separated directories to search for user defined fomatter classes' -r
complete --command t -n "__fish_seen_subcommand_from configure" -l default-formatter -d 'The format to use when display is invoked without a `--format` option. Default \'text\'' -r
complete --command t -n "__fish_seen_subcommand_from configure" -l note-editor -d 'Command to launch notes editor. Default: $EDITOR' -r
complete --command t -n "__fish_seen_subcommand_from configure" -l week-start -d 'The day of the week to use as the start of the week for t week. Default: monday' -r -f -a "{monday	,tuesday	,wednesday	,thursday	,friday	,saturday	,sunday	}"
complete --command t -n "__fish_seen_subcommand_from configure" -l interactive-entries -d 'How many unique previous notes to show when selecting interactively' -r
complete --command t -n "__fish_seen_subcommand_from configure" -l require-note -d 'Prompt for a note if one isn\'t provided when checking in (default)'
complete --command t -n "__fish_seen_subcommand_from configure" -l no-require-note -d 'Entries can be created without notes'
complete --command t -n "__fish_seen_subcommand_from configure" -l auto-checkout -d 'Checkout of current running entry when starting a new one'
complete --command t -n "__fish_seen_subcommand_from configure" -l no-auto-checkout -d 'Starting a new entry fails if one is running (default)'
complete --command t -n "__fish_seen_subcommand_from configure" -s h -l help -d 'Print help information'
complete --command t -n "__fish_seen_subcommand_from display" -s s -l start -d 'Include entries that start on this date or later' -r
complete --command t -n "__fish_seen_subcommand_from display" -s e -l end -d 'Include entries that start on this date or earlier' -r
complete --command t -n "__fish_seen_subcommand_from display" -s f -l format -d 'The output format. Valid built-in formats are chart, text, ical, csv, json and ids. Documentation on defining custom formats can be found at https://tiempo.categulario.xyz or the man page included with the installation.' -r
complete --command t -n "__fish_seen_subcommand_from display" -s g -l grep -d 'Only include entries whose note matches this regular expression' -r
complete --command t -n "__fish_seen_subcommand_from display" -s v -l ids -d 'Print database ids (for use with edit)'
complete --command t -n "__fish_seen_subcommand_from display" -s h -l help -d 'Print help information'
complete --command t -n "__fish_seen_subcommand_from today" -s e -l end -d 'Include entries that start on this date or earlier' -r
complete --command t -n "__fish_seen_subcommand_from today" -s f -l format -d 'The output format. Valid built-in formats are chart, text, ical, csv, json and ids. Documentation on defining custom formats can be found at https://tiempo.categulario.xyz or the man page included with the installation.' -r
complete --command t -n "__fish_seen_subcommand_from today" -s g -l grep -d 'Only include entries whose note matches this regular expression' -r
complete --command t -n "__fish_seen_subcommand_from today" -s v -l ids -d 'Print database ids (for use with edit)'
complete --command t -n "__fish_seen_subcommand_from today" -s h -l help -d 'Print help information'
complete --command t -n "__fish_seen_subcommand_from yesterday" -s f -l format -d 'The output format. Valid built-in formats are chart, text, ical, csv, json and ids. Documentation on defining custom formats can be found at https://tiempo.categulario.xyz or the man page included with the installation.' -r
complete --command t -n "__fish_seen_subcommand_from yesterday" -s g -l grep -d 'Only include entries whose note matches this regular expression' -r
complete --command t -n "__fish_seen_subcommand_from yesterday" -s v -l ids -d 'Print database ids (for use with edit)'
complete --command t -n "__fish_seen_subcommand_from yesterday" -s h -l help -d 'Print help information'
complete --command t -n "__fish_seen_subcommand_from week" -s e -l end -d 'Include entries that start on this date or earlier' -r
complete --command t -n "__fish_seen_subcommand_from week" -s f -l format -d 'The output format. Valid built-in formats are chart, text, ical, csv, json and ids. Documentation on defining custom formats can be found at https://tiempo.categulario.xyz or the man page included with the installation.' -r
complete --command t -n "__fish_seen_subcommand_from week" -s g -l grep -d 'Only include entries whose note matches this regular expression' -r
complete --command t -n "__fish_seen_subcommand_from week" -s v -l ids -d 'Print database ids (for use with edit)'
complete --command t -n "__fish_seen_subcommand_from week" -s h -l help -d 'Print help information'
complete --command t -n "__fish_seen_subcommand_from month" -s f -l format -d 'The output format. Valid built-in formats are chart, text, ical, csv, json and ids. Documentation on defining custom formats can be found at https://tiempo.categulario.xyz or the man page included with the installation.' -r
complete --command t -n "__fish_seen_subcommand_from month" -s g -l grep -d 'Only include entries whose note matches this regular expression' -r
complete --command t -n "__fish_seen_subcommand_from month" -s m -l month -d 'Include entries of the specified month instead of the current month' -r -f -a "{this	,current	,last	,jan	,january	,feb	,february	,mar	,march	,apr	,april	,may	,jun	,june	,jul	,july	,aug	,august	,sep	,september	,oct	,october	,nov	,november	,dic	,december	}"
complete --command t -n "__fish_seen_subcommand_from month" -s v -l ids -d 'Print database ids (for use with edit)'
complete --command t -n "__fish_seen_subcommand_from month" -s h -l help -d 'Print help information'
complete --command t -n "__fish_seen_subcommand_from in" -l at -d 'Use this time instead of now' -r
complete --command t -n "__fish_seen_subcommand_from in" -s h -l help -d 'Print help information'
complete --command t -n "__fish_seen_subcommand_from resume" -l at -d 'Use this time instead of now' -r
complete --command t -n "__fish_seen_subcommand_from resume" -l id -d 'Use entry with ID instead of the last entry' -r
complete --command t -n "__fish_seen_subcommand_from resume" -s i -l interactive -d 'Choose an entry of the (unique) last N interactively'
complete --command t -n "__fish_seen_subcommand_from resume" -s h -l help -d 'Print help information'
complete --command t -n "__fish_seen_subcommand_from out" -l at -d 'Use this time instead of now' -r
complete --command t -n "__fish_seen_subcommand_from out" -s h -l help -d 'Print help information'
complete --command t -n "__fish_seen_subcommand_from sheet" -s h -l help -d 'Print help information'
complete --command t -n "__fish_seen_subcommand_from list" -s a -l all -d 'List archive sheets also'
complete --command t -n "__fish_seen_subcommand_from list" -s f -l flat -d 'show only the sheet names'
complete --command t -n "__fish_seen_subcommand_from list" -s h -l help -d 'Print help information'
complete --command t -n "__fish_seen_subcommand_from kill" -l id -d 'Delete entry with this ID instead of sheet' -r
complete --command t -n "__fish_seen_subcommand_from kill" -s l -l last -d 'Delete the last entry of the current sheet'
complete --command t -n "__fish_seen_subcommand_from kill" -s h -l help -d 'Print help information'
complete --command t -n "__fish_seen_subcommand_from now" -s h -l help -d 'Print help information'
complete --command t -n "__fish_seen_subcommand_from edit" -l id -d 'Edit entry with this ID instead of the last one in the current sheet' -r
complete --command t -n "__fish_seen_subcommand_from edit" -s s -l start -d 'Set this as the start time' -r
complete --command t -n "__fish_seen_subcommand_from edit" -s e -l end -d 'Set this as the end time' -r
complete --command t -n "__fish_seen_subcommand_from edit" -s m -l move -d 'Move entry to another sheet' -r
complete --command t -n "__fish_seen_subcommand_from edit" -s z -l append -d 'Append to the current note instead of replacing it. The delimiter between appended notes is configurable (see configure)'
complete --command t -n "__fish_seen_subcommand_from edit" -s h -l help -d 'Print help information'

set -l sheetcommandslong display kill sheet week month
set -l sheetcommandsshort d k s w m
set -l sheetcommandsall $sheetcommandslong $sheetcommandsshort

# complete sheet name
complete --command t --condition "__fish_seen_subcommand_from $sheetcommandsall" -a "(t l --all --flat)"
